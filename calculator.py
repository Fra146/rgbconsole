import RGBConsole
from termcolor import colored
import time

possible_operator = ["+", "-", "*", "/"]

def main():
  print(colored('Calculator by Fra146', 'green'))
  start = input("Press enter to start ")
  calculator()

def calculator():
  select = input(colored("Select an operation: +, -, *, / or insert exit to return to console: ", 'green'))
  if select in possible_operator:
    num1 = input("First number: ")
    num2 = input("Second number: ")
    if select == "+":
      print("The result is: " + num1 + num2)
      calculator()
    elif select == "-":
      print("The result is: " + num1 - num2)
      calculator()
    elif select == "*":
      print("The result is: " + num1 * num2)
      calculator()
    elif select == "/":
      print('The result is: ' + num1 / num2)
      calculator()
    else:
      print("Oh, I'm really sorry about that, but you found a bug, contact 9ㄣƖɐɹℲ#9239 on discord please!")
  elif select == 'exit':
    print("Bye")
    time.sleep(0.5)
    RGBConsole.command_selector()
  
  else:
    print(colored('Error', 'red'))
    calculator()