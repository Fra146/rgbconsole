import calculator
from termcolor import colored
import time
import sys

possible_command = ["help", "about", "calculator", "exit"]

#serve per mostrare la scritta dopo il comando about
def about():
	print(colored('RGBConsole by Fra146, if you want to report a bug contact me on discord 9ㄣƖɐɹℲ#9239', 'green'))
	time.sleep(0.1)
	command_selector()


#serve per mostrare la scritta dopo il comando help
def help():
	print(colored('HELP, CALCULATOR, ABOUT, EXIT', 'red'))
	time.sleep(0.1)
	command_selector()

#il nucleo della console, da qui si selezionano i comandi
def command_selector():
	global command
	command = input(">> ")
	software()

#serve per aprire i programmi quando vengono chiamati
def software():
	if command in possible_command:
		if command == "help":
			help()
		elif command == "about":
			about()
		elif command == "calculator":
			calculator.main()
		elif command == "exit":
			print("Good bye!")
			time.sleep(0.5)
		else:
			print("Oh, I'm really sorry about that, but you found a bug, contact 9ㄣƖɐɹℲ#9239 on discord please!")
	else:
		print("No such program")
		command_selector()
		


#gestisce il caricamento
def start():
	print(colored('R', 'red'), colored('G', 'green'), colored('B', 'blue'), colored('Console by Fra146', 'white'))
	start = input("Press enter to start the console")
	print("Loading...")
	for i in range(0, 100):
		time.sleep(0.01)
		sys.stdout.write(u"\u001b[1000D" + str(i + 1) + "%")
		sys.stdout.flush()
	print()


#esegue le funzioni di base
def main():
	start()
	command_selector()


#Fa partire il programma
#if __name__ == "__main__":
#    main()
main()